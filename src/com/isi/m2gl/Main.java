package com.isi.m2gl;

import com.isi.m2gl.domain.Activite;

public class Main {

    public static void main(String[] args) {
        String choix = args[0];
        String nom = args[1];
        Integer duree = Integer.parseInt(args[2]);
        Activite activite = ActiviteFactory.build(choix, nom, duree);
        if(activite == null) {
            System.out.println("Activité non connue");
            return;
        }
        System.out.println("activite = " + activite);
        String message = String.format("Activité du jour: %s durée %s", activite.getNom(), activite.getDuree());
        System.out.println(message);
    }
}
