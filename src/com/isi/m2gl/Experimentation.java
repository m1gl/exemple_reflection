package com.isi.m2gl;

import com.isi.m2gl.domain.Activite;
import com.isi.m2gl.domain.Cours;

import java.lang.reflect.InvocationTargetException;

public class Experimentation {
    public static void main(String[] args) {
        System.out.println("Code running .....");
        Cours cours = new Cours("test", 2);
        System.out.println("cours = " + cours);
        Class<?> aClass = cours.getClass();
        String nomClasse = ActiviteFactory.getNomClasse("cours");
        try {
            Class<?> maClasse = Class.forName(nomClasse);
            Object o = maClasse.getConstructors()[0].newInstance("sqwsdfqsfd", 2);
            Cours c = (Cours) o;
            System.out.println("c.getNom() = " + c.getNom());
            //maClasse.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("nom de la classe = " + aClass.getName());
        System.out.println("Last runned intruction");

    }
}
