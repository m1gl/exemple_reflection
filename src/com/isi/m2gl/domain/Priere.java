package com.isi.m2gl.domain;

public class Priere implements Activite {
    private String nom;
    private int duree;

    public Priere(String nom, int duree) {
        this.nom = nom;
        this.duree = duree;
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public int getDuree() {
        return duree;
    }

    @Override
    public String toString() {
        return "Priere{" +
                "nom='" + nom + '\'' +
                ", duree=" + duree +
                '}';
    }
}
