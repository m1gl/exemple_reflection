package com.isi.m2gl.domain;

public class Travail implements Activite {
    private String nom;
    private int duree;

    public Travail(String nom, int duree) {
        this.nom = nom;
        this.duree = duree;
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public int getDuree() {
        return duree;
    }
}
