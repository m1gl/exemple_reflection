package com.isi.m2gl.domain;

public class SportCollectif extends Sport {
    public SportCollectif(String nom, int duree) {
        super(nom, duree);
    }

    @Override
    public String getNom() {
        return super.getNom();
    }

    @Override
    public int getDuree() {
        return super.getDuree();
    }
}
