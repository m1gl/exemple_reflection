package com.isi.m2gl.domain;

public interface Activite {
    String getNom();
    int getDuree();
}
