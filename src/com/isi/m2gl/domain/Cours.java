package com.isi.m2gl.domain;

public class Cours implements Activite {
    private String nom;
    private int duree;

    public Cours(String nom, int duree) {
        this.nom = nom;
        this.duree = duree;
    }

    public String getNom() {
        return nom;
    }

    public int getDuree() {
        return duree;
    }

    @Override
    public String toString() {
        return "Cours{" +
                "nom='" + nom + '\'' +
                ", duree=" + duree +
                '}';
    }
}
