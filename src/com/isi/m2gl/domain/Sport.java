package com.isi.m2gl.domain;

public class Sport implements Activite {
    private String nom;
    private int duree;

    public Sport(String nom, int duree) {
        this.nom = nom;
        this.duree = duree;
    }

    public String getNom() {
        return nom;
    }

    public int getDuree() {
        return duree;
    }

    @Override
    public String toString() {
        return "Sport{" +
                "type='" + nom + '\'' +
                ", duree=" + duree +
                '}';
    }
}
