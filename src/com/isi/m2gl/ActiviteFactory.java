package com.isi.m2gl;

import com.isi.m2gl.domain.Activite;

import java.lang.reflect.InvocationTargetException;

import static com.sun.xml.internal.ws.util.StringUtils.capitalize;

public class ActiviteFactory {
    public static Activite build(String choix, String nom, int duree){
        //new choix (nom, duree);

        String nomClasse = getNomClasse(choix);
        try {
            Class<?> aClass = Class.forName(nomClasse);
            Object newInstance = aClass.getConstructors()[0].newInstance(nom, duree);

            return (Activite) newInstance;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

//        if("cours".equals(choix)){
//            return new Cours(nom, duree);
//        }
//        else if("sport".equals(choix)){
//            return new Sport(nom, duree);
//        }
//        else if("travail".equals(choix)){
//            return new Travail(nom, duree);
//        }
        return null;
    }

    public static String getNomClasse(String choix) {
        return "com.isi.m2gl.domain." + capitalize(choix);
    }
}
